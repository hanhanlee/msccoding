# README #

This repository is for MSCCoding part.

### Description of the code ###

This is simple java project that calculates tax and total prices.
When the application gets the Order List, it calculates each Order's tax and prices.
And also it will show the grand total.

Order - This class would have OrderLine linked hash map, because of the order.
OrderLine - This class would have Item and Quantity object.
Item - This class would have Description and Price of the item.
Calculator - It calculates tax, total and grand total.
CalcResult - This class is for the result of the calculation. Although I used this class for unit test only, but I can modify this class in case of I need the output for the calculation.

### Tax Calculation Rule ###

Tax calculation - Based on the description of the item, it calculates the tax.
If the item is imported one, tax would be 15%, else it would be 10%.


### What is this repository for? ###

* We would ask you to refactor the code to make it easier to maintain, 
* writing clean code with meaningful names, OOP principle etc..  
* and writing unit tests to provide a good coverage of the code
* 1.00


### Who do I talk to? ###

* Han Lee