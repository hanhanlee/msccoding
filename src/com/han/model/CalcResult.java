package com.han.model;

/**
 * CalcResult class for storing each order's total tax and total
 * @author Han Lee
 *
 */
public class CalcResult {

	double totalTax;
	double total;
	
	public CalcResult(double totalTax, double total) {
		this.total = total;
		this.totalTax = totalTax;
	}
	
	public double getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}	
}
