package com.han.model;

/**
 * represents an order line which contains the Item and the quantity.
 * @author - Han Lee
 */
public class OrderLine {

	private int quantity;
	private Item item;

	/**
	 * 
	 * @param item Item of the order
	 * @param quantity Quantity of the item
	 * @throws Exception
	 */
	public OrderLine(Item item, int quantity) throws Exception {
		if (item == null) {
			System.err.println("ERROR - Item is NULL");
			throw new Exception("Item is NULL");
		} else if (quantity < 0) {
			System.err.println("ERROR - Quantity is NEGATIVE");
			throw new IllegalArgumentException("Quantity is NEGATIVE");
		}
		
		this.item = item;
		this.quantity = quantity;
	}

	public Item getItem() {
		return item;
	}

	public int getQuantity() {
		return quantity;
	}
}