package com.han.test;

import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.han.main.Calculator;
import com.han.model.Item;
import com.han.model.Order;
import com.han.model.OrderLine;

public class calculatorTest {

	private Item item1;
	private Item item2;
	
	private Order order1;
	private Order order2;
	
	private OrderLine orderLine1;
	private OrderLine orderLine2;
	
	private Calculator calc;
	
	@Before
	public void setUp() throws Exception {
		calc = new Calculator();
		item1 = new Item("wine", (float) 15.22);
		item2 = new Item("imported wine", (float) 15.22);
		
		orderLine1 = new OrderLine(item1, 3);
		orderLine2 = new OrderLine(item2, 2);
		
		order1 = new Order();
		order2 = new Order();
	}
	
	@Test (expected = IllegalArgumentException.class)
	@Ignore
	public void addNullOrderTest() throws Exception {
		order2.add(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	@Ignore
	public void addNegativeOrderTest() throws Exception {
		OrderLine temp = new OrderLine(item1, -3);
		order2.add(temp);
	}
	
	@Test
	public void calculateTest() throws Exception {
		Map<String, Order> map = new LinkedHashMap<String, Order>();
		order1.add(orderLine1);
		map.put("Order 1", order1);
		
		order2.add(orderLine1);
		order2.add(orderLine2);
		
		map.put("Order 2", order2);
		
		calc.calculate(map);
		
		// Test total tax of order 1
		double tax1 = item1.getPrice() * 0.10 * orderLine1.getQuantity();
		assertEquals(calc.getMap().get("Order 1").getTotalTax(), tax1, 0.01);
			
		// Test total tax of order 2
		double tax2 = item1.getPrice() * 0.10 * orderLine1.getQuantity() + item2.getPrice() * 0.15 * orderLine2.getQuantity();
		assertEquals(calc.getMap().get("Order 2").getTotalTax(), Math.round(tax2 * 100.0) / 100.0, 0.01);

		// Test total of order 1
		double total1 = item1.getPrice() * orderLine1.getQuantity();
		assertEquals(calc.getMap().get("Order 1").getTotal(), total1, 0.01);
		
		// Test total of order 2
		double total2 = item1.getPrice() * orderLine1.getQuantity() + item2.getPrice() * orderLine2.getQuantity();
		assertEquals(calc.getMap().get("Order 2").getTotal(), total2, 0.01);
	}
}
